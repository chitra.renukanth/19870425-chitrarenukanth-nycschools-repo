package com.chitrar.nycschools.view;

import static androidx.navigation.Navigation.findNavController;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.chitra.nycschools.viewmodel.NYCActivityViewModel;
import com.chitrar.nycschools.R;
import com.chitrar.nycschools.databinding.ActivityMainBinding;

public class NYCMainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        NYCActivityViewModel viewModel = new ViewModelProvider(this).get(NYCActivityViewModel.class);
        NavHostFragment navHostfragment = (NavHostFragment)getSupportFragmentManager().findFragmentById(R.id.nycNavHostFragment);
        NavController navController = navHostfragment.getNavController();
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
    }

}


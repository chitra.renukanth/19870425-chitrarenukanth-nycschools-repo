package com.chitrar.nycschools.view.fragments;

import static androidx.navigation.fragment.FragmentKt.findNavController;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.chitra.nycschools.viewmodel.NYCActivityViewModel;
import com.chitrar.nycschools.R;
import com.chitrar.nycschools.databinding.FragmentSchoolDetailBinding;
import com.chitrar.nycschools.model.NycSchoolDetail;

public class SchoolDetailFragment extends Fragment {

    private NYCActivityViewModel activityViewModel;
    private FragmentSchoolDetailBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityViewModel = new ViewModelProvider(requireActivity()).get(NYCActivityViewModel.class);
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                findNavController(SchoolDetailFragment.this).popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSchoolDetailBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        intiUI();
    }

    private void intiUI() {
        NycSchoolDetail schoolDetail = activityViewModel.getNycSchoolDetail();
        binding.schoolNameHeaderTxt.setText(schoolDetail.getSchoolName() != null ? schoolDetail.getSchoolName() : "");
        binding.numOfSatUserValue.setText(schoolDetail.getSatTestTakers() != null ? schoolDetail.getSatTestTakers() : getString(R.string.not_available));
        binding.mathScoreValue.setText(schoolDetail.getSatMathScore() != null ? schoolDetail.getSatMathScore() : getString(R.string.not_available));
        binding.readingScoreValue.setText(schoolDetail.getSatReadingScore() != null ? schoolDetail.getSatReadingScore() : getString(R.string.not_available));
        binding.writingScoreValue.setText(schoolDetail.getSatWritingScore() != null ? schoolDetail.getSatWritingScore() : getString(R.string.not_available));
    }
}

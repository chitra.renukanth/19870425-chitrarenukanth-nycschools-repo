package com.chitra.nycschools.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.chitrar.nycschools.model.NycSchool
import com.chitrar.nycschools.model.NycSchoolDetail
import com.chitrar.nycschools.repository.NycSchoolRepository
import kotlinx.coroutines.launch

class NYCActivityViewModel : ViewModel() {

    val nycSchoolListObservable: MutableLiveData<MutableList<NycSchool>> = MutableLiveData()
    val nycSchoolListErrorObservable: MutableLiveData<Boolean> = MutableLiveData()
    var nycSchoolDetail: NycSchoolDetail? = null
    var nycSchoolLists: List<NycSchool>  = ArrayList()
    val nycSchoolDetailObservable: SingleLiveData<Boolean> = SingleLiveData()

    fun getNycSchoolList() {
        viewModelScope.launch {
            NycSchoolRepository().getSchoolList(object :
                NycSchoolRepository.ApiResponse<MutableList<NycSchool>> {
                override fun onSuccess(response: MutableList<NycSchool>) {
                    nycSchoolLists = response
                    nycSchoolListObservable.value = response
                }

                override fun onError() {
                    nycSchoolListErrorObservable.value = true
                }
            })
        }
    }

    fun getNycSchoolDetail(dbn: String) {
        viewModelScope.launch {
            NycSchoolRepository().getSchoolDetail(dbn, object :
                NycSchoolRepository.ApiResponse<List<NycSchoolDetail>> {
                override fun onSuccess(response: List<NycSchoolDetail>) {
                    if (response.isNotEmpty()) {
                        nycSchoolDetail = response[0]
                        nycSchoolDetailObservable.postValue(true)
                    } else {
                        nycSchoolDetailObservable.postValue(false)
                    }
                }

                override fun onError() {
                    nycSchoolDetailObservable.postValue(false)
                }
            })
        }
    }

    class SingleLiveData<T> : MutableLiveData<T?>() {

        override fun observe(owner: LifecycleOwner, observer: Observer<in T?>) {
            super.observe(owner, Observer { t ->
                if (t != null) {
                    observer.onChanged(t)
                    postValue(null)
                }
            })
        }
    }
}
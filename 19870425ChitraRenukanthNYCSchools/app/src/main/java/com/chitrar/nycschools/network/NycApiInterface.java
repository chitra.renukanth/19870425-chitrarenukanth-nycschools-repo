package com.chitrar.nycschools.network;

import com.chitrar.nycschools.model.NycSchool;
import com.chitrar.nycschools.model.NycSchoolDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NycApiInterface {

    @GET("s3k6-pzi2.json?$select=dbn,school_name")
    Call<List<NycSchool>> doGetSchoolList();

    @GET("f9bf-2cp4.json")
    Call<List<NycSchoolDetail>> doGetSchoolDetail(@Query("dbn") String dbnId);

}

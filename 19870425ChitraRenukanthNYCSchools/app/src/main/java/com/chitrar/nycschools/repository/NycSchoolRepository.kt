package com.chitrar.nycschools.repository

import android.util.Log
import com.chitrar.nycschools.model.NycSchool
import com.chitrar.nycschools.model.NycSchoolDetail
import com.chitrar.nycschools.network.NycApiClient
import com.chitrar.nycschools.network.NycApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NycSchoolRepository {

    var apiInterface: NycApiInterface = NycApiClient.getClient().create(NycApiInterface::class.java)

    fun getSchoolList(onResponse: ApiResponse<MutableList<NycSchool>>) {
        var getSchoolListCall = apiInterface.doGetSchoolList()
        getSchoolListCall.enqueue(object : Callback<MutableList<NycSchool>> {
            override fun onResponse(
                call: Call<MutableList<NycSchool>>,
                response: Response<MutableList<NycSchool>>
            ) {
                response.body()?.let { onResponse.onSuccess(it) }
            }

            override fun onFailure(call: Call<MutableList<NycSchool>>, t: Throwable) {
                call.cancel()
                onResponse.onError()
            }
        })
    }

    fun getSchoolDetail(dbnId: String, onResponse: ApiResponse<List<NycSchoolDetail>>) {
        var getSchoolDetailCall = apiInterface.doGetSchoolDetail(dbnId)
        getSchoolDetailCall.enqueue(object : Callback<List<NycSchoolDetail>> {
            override fun onResponse(
                call: Call<List<NycSchoolDetail>>,
                response: Response<List<NycSchoolDetail>>
            ) {
                response.body()?.let { onResponse.onSuccess(it) }
            }

            override fun onFailure(call: Call<List<NycSchoolDetail>>, t: Throwable) {
                call.cancel()
                onResponse.onError()
            }
        })
    }

    interface ApiResponse<T> {
        fun onSuccess(response: T)
        fun onError()
    }
}
package com.chitrar.nycschools.view.fragments;

import static androidx.navigation.fragment.FragmentKt.findNavController;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.chitra.nycschools.viewmodel.NYCActivityViewModel;
import com.chitrar.nycschools.R;
import com.chitrar.nycschools.databinding.FragmentSchoolListBinding;
import com.chitrar.nycschools.model.NycSchool;
import com.chitrar.nycschools.view.adapter.SchoolListAdapter;

import java.util.List;

public class SchoolListFragment extends Fragment {

    private NYCActivityViewModel activityViewModel;
    private FragmentSchoolListBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityViewModel = new ViewModelProvider(requireActivity()).get(NYCActivityViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSchoolListBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(activityViewModel.getNycSchoolLists() != null && !activityViewModel.getNycSchoolLists().isEmpty()){
            setAdapter(activityViewModel.getNycSchoolLists());
        } else {
            binding.progressBarLayout.setVisibility(View.VISIBLE);
            binding.circularProgressBar.setVisibility(View.VISIBLE);
            setUpObservers();
            activityViewModel.getNycSchoolList();
        }
    }

    private void setUpObservers() {
        activityViewModel.getNycSchoolListObservable().observe(getViewLifecycleOwner(), schoolList -> {
            binding.progressBarLayout.setVisibility(View.GONE);
            if (schoolList != null && !schoolList.isEmpty()) {
                setAdapter(schoolList);
            } else {
                binding.errorMessage.setVisibility(View.VISIBLE);
                binding.errorMessage.setText(getString(R.string.empty_message));
            }
        });

        activityViewModel.getNycSchoolListErrorObservable().observe(getViewLifecycleOwner(), error -> {
            binding.circularProgressBar.setVisibility(View.GONE);
            binding.errorMessage.setVisibility(View.VISIBLE);
        });
    }

    private void setAdapter(List<NycSchool> schoolList) {
        binding.schoolListRecyclerView.setVisibility(View.VISIBLE);
        SchoolListAdapter adapter = new SchoolListAdapter(schoolList, dbn -> {
            if(dbn != null) {
                binding.circularProgressBar.setVisibility(View.VISIBLE);
                activityViewModel.getNycSchoolDetail(dbn);
                setSchoolDetailObserver();
            } else {
                showToastMessage();
            }
        });
        binding.schoolListRecyclerView.setAdapter(adapter);
    }

    private void setSchoolDetailObserver(){
        activityViewModel.getNycSchoolDetailObservable().observe(getViewLifecycleOwner(), isSuccess -> {
            binding.circularProgressBar.setVisibility(View.GONE);
            if(isSuccess){
                findNavController(this).navigate(R.id.ToSchoolDetailFragment);
            } else {
                showToastMessage();
            }
            activityViewModel.getNycSchoolDetailObservable().removeObservers(getViewLifecycleOwner());
        });
    }

    private void showToastMessage() {
        Toast.makeText(getContext(), R.string.empty_message, Toast.LENGTH_SHORT).show();
    }

}
